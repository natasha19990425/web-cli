#!/usr/bin/env node

import { program } from 'commander';
import { execSync } from 'child_process';

program
  .command('new <projectName>')
  .description('Create a new Angular Project')
  .action((projectName) => {
    // 創建一個新的 Angular Project
    execSync(`ng new ${projectName} --standalone `, { stdio: 'inherit' });

});

program
  .command('c <component>')
  .description('Create a Component')
  .action((componentName) => {
    // 創建一個 Component
    execSync(`ng g component ${componentName} --standalone `, { stdio: 'inherit' });

});

program.parse(process.argv);